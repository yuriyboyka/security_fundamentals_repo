import struct

from Crypto.Cipher import ARC4
from dh import create_dh_key, calculate_dh_secret
from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from lib.crypto_utils import ANSI_X923_pad, ANSI_X923_unpad            
from base64 import b64encode, b64decode
# stream or block
# encryption_type = 'block'


class StealthConn(object):
    def __init__(self, conn, client=False, server=False, verbose=False):
        self.shared_hash = b''
        
        self.conn = conn
        self.cipher = None
        self.client = client
        self.server = server
        self.verbose = verbose
        self.initiate_session()
        self.src_send_seq = 0
        self.src_recv_seq = 0
        self.dst_send_seq = 0
        self.dst_recv_seq = 0

    def initiate_session(self):
        # Perform the initial connection handshake for agreeing on a shared secret
        # TODO: Your code here!
        # This can be broken into code run just on the server or just on the client
        if self.server or self.client:
            my_public_key, my_private_key = create_dh_key()
            # Send them our public key
            self.send(bytes(str(my_public_key), "ascii"))
            # Receive their public key
            their_public_key = int(self.recv())
            # Obtain our shared secret
            self.shared_hash = calculate_dh_secret(their_public_key, my_private_key).encode()
            print("Shared hash: {}".format(self.shared_hash))
        # We are using AES 256， Block size of AES is 128。
        
            self.shared_hash = self.shared_hash[:32]
            self.cipher = AES.new(self.shared_hash, AES.MODE_CBC, self.shared_hash[:16])

    def send(self, data):
        if self.cipher:
            self.src_send_seq += len(data)
            hmac = HMAC.new(key=self.shared_hash, msg=data).digest()
            pad_data = ANSI_X923_pad(data, AES.block_size)
            en_data = b64encode(self.src_send_seq.to_bytes(4, 'little')) + b64encode(hmac) + pad_data
            encrypted_data = self.cipher.encrypt(en_data)
            if not __debug__:
                print("our sending sequence is: ", self.src_send_seq)
            if self.verbose:
                print("Original data: {}".format(data))
                print("Encrypted data: {}".format(repr(encrypted_data)))
                print("Sending packet of length {}".format(len(encrypted_data)))
        else:
            encrypted_data = data

        # Encode the data's length into an unsigned two byte int ('H')
        pkt_len = struct.pack('H', len(encrypted_data))
        self.conn.sendall(pkt_len)
        self.conn.sendall(encrypted_data)

    def recv(self):
        # Decode the data's length from an unsigned two byte int ('H')
        pkt_len_packed = self.conn.recv(struct.calcsize('H'))
        unpacked_contents = struct.unpack('H', pkt_len_packed)
        pkt_len = unpacked_contents[0]
        encrypted_data = self.conn.recv(pkt_len)
        if self.cipher:
            en_data = self.cipher.decrypt(encrypted_data)
            
            self.dst_send_seq = int.from_bytes(b64decode(en_data[:8]), 'little')
            
            dst_hmac = b64decode(en_data[8:32])
            data = ANSI_X923_unpad(en_data[32:], AES.block_size)
            src_hmac = HMAC.new(key=self.shared_hash, msg=data).digest()
                
            if src_hmac != dst_hmac:
                print("Message has been modified in the midway")
            else:
                print("message is correct")
            self.src_recv_seq += len(data)
            
            if self.verbose:
                print("Receiving packet of length {}".format(pkt_len))
                print("Encrypted data: {}".format(repr(encrypted_data)))
                print("Original data: {}".format(data))
            if not __debug__:
                print(" sending sequence is: ", self.dst_send_seq)
                print("our receiver's sequence is: ", self.src_recv_seq)
            if self.src_recv_seq != self.dst_send_seq:
                print("Sequence is wrong")
            else:
                print("Sequence was match")
            
        else:
            data = encrypted_data

        return data

   

    
    def close(self):
        self.conn.close()
